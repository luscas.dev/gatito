import React, { useState, useEffect } from "react";
import { View, TextInput, FlatList, TouchableOpacity } from "react-native";

import {
  Header,
  Cart,
  TotalCart,
  TotalCartTitle,
  TotalCartPrice,
  CheckoutButton,
  CheckoutButtonLabel,
  Product,
  ProductTitle,
  ProductDescription,
  ProductQuantity,
  ProductTotal,
  ProductTotalPrice,
  ProductPrice,
  ProductButton,
  ProductButtonTitle,
} from "./styles";

const CartScreen = () => {
  const [totalCart, setTotalCart] = useState(0);
  const [products, setProduct] = useState([
    {
      id: 1,
      title: "Banho",
      description: "Realizamos a higiene completa do seu pet",
      quantity: 5,
      price: 79.9,
    },
    {
      id: 2,
      title: "Vacina V4",
      description: "Himunize seu pet de várias doenças",
      quantity: 2,
      price: 179.9,
    },
    {
      id: 3,
      title: "Vacina Antirrábica",
      description: "Vacina contra raiva",
      quantity: 1,
      price: 89.9,
    },
  ]);

  const removeProductItem = (item) => {
    let index = products.indexOf(item);
    products.splice(index, 1);
    setProduct([...products]);

    changeTotalCart();
  };

  const changeProductQuantity = (item, quantity) => {
    let index = products.indexOf(item);
    products[index].quantity = quantity;
    setProduct([...products]);

    changeTotalCart();
  };

  const changeTotalCart = () => {
    setTotalCart(
      products.reduce((prev, curr) => prev + curr.quantity * curr.price, 0)
    );
  };

  useEffect(() => {
    changeTotalCart();
  }, [totalCart]);

  const ProductItem = ({ item }) => (
    <Product>
      <View>
        <ProductTitle>{item.title}</ProductTitle>
        <ProductDescription>{item.description}</ProductDescription>

        <ProductQuantity>
          Quantidade:{" "}
          <TextInput
            defaultValue={item.quantity}
            onChange={(e) => changeProductQuantity(item, e.target.value)}
          />
        </ProductQuantity>
        <ProductTotal>
          Total:{" "}
          <ProductTotalPrice>
            R$ {Math.round(item.quantity * item.price).toFixed(2)}
          </ProductTotalPrice>
        </ProductTotal>
      </View>
      <View>
        <ProductPrice>R$ {item.price.toFixed(2)}</ProductPrice>
        <TouchableOpacity onPress={() => removeProductItem(item)}>
          <ProductButton>
            <ProductButtonTitle>Remover do Carrinho</ProductButtonTitle>
          </ProductButton>
        </TouchableOpacity>
      </View>
    </Product>
  );

  return (
    <View>
      <Header>
        <Cart>
          <TotalCart>
            <TotalCartTitle>Total do Carrinho:</TotalCartTitle>
            <TotalCartPrice>R$ {totalCart.toFixed(2)}</TotalCartPrice>
          </TotalCart>
          <TouchableOpacity
            activeOpacity={0.85}
            onPress={() => alert("Pedido realizado!")}
          >
            <CheckoutButton>
              <CheckoutButtonLabel>Concluir Pedido</CheckoutButtonLabel>
            </CheckoutButton>
          </TouchableOpacity>
        </Cart>
      </Header>

      <FlatList
        data={products}
        keyExtractor={(item) => item.id.toString()}
        renderItem={ProductItem}
      />
    </View>
  );
};

export default CartScreen;
