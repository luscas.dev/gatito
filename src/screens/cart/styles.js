import { colors } from "../../theme";
import styled from "styled-components/native";

export const Header = styled.View`
  background-color: ${colors.purple};
  width: 100%;
  padding: 15px 25px;
`;
export const Cart = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
export const TotalCart = styled.View`
  flex-direction: column;
  padding: 10px 0;
`;
export const TotalCartTitle = styled.Text`
  font-size: 14px;
  color: #fff;
`;
export const TotalCartPrice = styled.Text`
  font-size: 16px;
  font-weight: 500;
  color: ${colors.yellow};
`;
export const CheckoutButton = styled.View`
  background-color: ${colors.yellow};
  border-radius: 3px;
  elavation: 2;
`;
export const CheckoutButtonLabel = styled.Text`
  font-weight: 500;
  color: ${colors.purple};
  padding: 10px 15px;
`;

export const Product = styled.View`
  background-color: #fff;
  width: 100%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 25px;
  margin-bottom: 1px;
`;
export const ProductTitle = styled.Text`
  font-weight: 500;
  font-size: 16px;
  color: ${colors.yellow};
`;
export const ProductDescription = styled.Text`
  font-size: 14px;
  color: #888;
`;
export const ProductQuantity = styled.Text`
  font-size: 14px;
  color: #666;
  margin-top: 10px;
`;
export const ProductTotal = styled.Text`
  font-size: 14px;
  color: #666;
`;
export const ProductTotalPrice = styled.Text`
  font-weight: 500;
  color: ${colors.purple};
`;
export const ProductPrice = styled.Text`
  font-size: 16px;
  color: ${colors.purple};
  text-align: center;
`;
export const ProductButton = styled.View`
  max-width: 130px;
  background-color: ${colors.purple};
  padding: 10px 15px;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
  margin-top: 10px;
`;
export const ProductButtonTitle = styled.Text`
  font-weight: 500;
  color: ${colors.yellow};
  text-align: center;
`;
